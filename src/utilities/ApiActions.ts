import axios from 'axios'
import { takeEvery, put, call, take, select, takeLatest } from 'redux-saga/effects'

class ApiAction {

  private instance;
  private static baseUrl = `https://api.tfl.gov.uk/`;

  constructor() {
    this.instance = axios.create();
    this.instance.interceptors.request.use(this.requestInterceptor);
  }

  requestInterceptor = (config) => {

    // const accessToken = self.window.localStorage.getItem('accessToken');

    // config.headers = {
    //   Authorization: `Bearer ${accessToken}`
    // }
    return config;
  }


  getServiceStatus = async () => {
    const endpoint = `${ApiAction.baseUrl}Line/Mode/tube,overground,dlr/Status?detail=true`
    return await this.instance.get(endpoint);
  }


  searchBikePoints = async (searchTerm: string) => {

    const encodedSerachTerm = encodeURIComponent(searchTerm);
    const endpoint = `${ApiAction.baseUrl}BikePoint/Search?query=${encodedSerachTerm}`

    return await this.instance.get(endpoint);

    }

}

const apiAction = new ApiAction();
export default apiAction;