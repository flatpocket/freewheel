import {takeEvery, put, select } from 'redux-saga/effects'
import {START_REQUEST, successfulRequest, ILineResponse} from '../actions/LineStatusActions'
import {START_SEARCH, successfulSearch} from '../actions/BikeSearchActions'

import ApiActions from '../utilities/ApiActions'


export function* startLineStatusRequest() {

  const response: any = yield ApiActions.getServiceStatus();

  const dto = response.data.map(r => {
    return({

        _id: r.id,
        name: r.name,
        mode: r.modeName,
        nightShift: r.serviceTypes.filter(lineStatus => lineStatus.name === 'Night').length > 0,
        lineStatus: r.lineStatuses

    } as ILineResponse)
  })

  yield put(successfulRequest(dto));

}
  
export function* startSearchRequest(term) {

  const { previousSearchTerm, results } = yield select((state: any) => state.bikeLocation);
  
  if(previousSearchTerm === term.payload)
    return results;

  const response: any = yield ApiActions.searchBikePoints(term.payload);

  const dto = response.data.map(r => {
    return {
      id: r.id.replace(/[a-zA-Z-]+_/g, ""),
      commonName: r.commonName,
      lat: r.lat,
      lon: r.lon
    }
  })

  yield put(successfulSearch(dto));

}

  export function* watchIncrementAsync() {
    yield takeEvery(START_REQUEST, startLineStatusRequest);
    yield takeEvery(START_SEARCH, startSearchRequest)
  }
