import * as React from "react";
import './main.css'

interface IAuthState {
}

interface IAuthProps {
    bikeLocations: any[]
}


const BikeLocations = ({ BikeLocations: { results, searchTerm } }) => {

    // const  = props;

    const renderNoResults = () => {
        return (
            <div>
                {results.length < 1 &&
                    <span> No bike points found for {searchTerm}</span>
                }
            </div>
        )
    }


    const renderHasResults = () => {
        return (

            <div>

                {results.map(bl => {
                    return (
                        <div>
                            {bl.id} {bl.commonName}, ({bl.lat}, {bl.lon})
                    </div>
                    )
                })
                }
            </div>
        )
    }


    return (
        <div>
            <h4>Bike Results</h4>
            {results.length < 1 && renderNoResults()}
            {results.length > 0 && renderHasResults()}
        </div>
    )

}

export default BikeLocations;