
import * as React from "react";
import * as ReactDOM from "react-dom";
import {ILineResponse} from '../../actions/LineStatusActions'
import * as _ from 'lodash'
import Search from '../../components/Search'
import './main.css'

interface IAuthState {
}

interface IAuthProps {
    lineDetails: Object,
    onLineClick(l): void
}


class LineSummary extends React.Component<IAuthProps, IAuthState>{

    constructor(props, state){
        super(props, state);
    }
  
    renderLine = (lines) => {

        return lines.sort((a, b) => a.name > b.name ? 1 : -1).map((line, key) => {

            const style: React.CSSProperties =  line.lineStatus.filter(lineStatus => lineStatus.statusSeverity != 10).length > 0 && {backgroundColor: 'red'};

            return(
                <div key={key} style={{...style}} className={'lineSummary-item'} 
                    onClick={() => this.props.onLineClick(line)}>

                    <div> {line.mode.toUpperCase()} - {line.name} </div>
                    {line.nightShift && <div className={'nightShift'}></div> }
                    
                </div>
            )

        });

    }

    render(){

        const {lineDetails} = this.props;

        return(
            <div className={'lineSummary-wrapper'}>
                {Object.keys(lineDetails).map((key, index) => {
                    return (
                        this.renderLine(lineDetails[key])
                    )
                })}
            </div>
        );
    }

}


export default LineSummary;
