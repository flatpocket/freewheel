
import * as React from "react";
import * as ReactDOM from "react-dom";
import { connect } from 'react-redux'
import { startRequest, ILineStatusState } from '../../actions/LineStatusActions'
import LineSummary from '../LineSummary'
import { startSearch } from '../../actions/BikeSearchActions'
import Search from '../../components/Search'
import BikeLocations from '../../containers/BikeLocations'

import './main.css'

interface IAuthState {
    detailLine,
    contentView

}

interface IAuthProps {
    startRequest(): void,
    startSearch(searchTerm): void
    lineDetails: ILineStatusState,
    history: any,
    bikeLocations: any

}


class TransportDelivery extends React.Component<IAuthProps, IAuthState>{

    constructor(props, state) {
        super(props, state);
        this.state = {
            detailLine: null,
            contentView: ''
        }
    }

    componentDidMount = () => {
        this.props.startRequest();
    }


    onLineClick = (line) => {
        console.log(line);
        this.setState((newState, prevState) => {
            return ({
                detailLine: line,
                contentView: 'LINE_STATUS'
            })
        })
    }

    updateContentView = (newView: string) => {

        this.setState((newState, prevState) => {
            return ({
                contentView: newView
            })
        })
    }

    renderDetailLine = (line) => {

        const isHighDisruptive = line && line.lineStatus.filter(lineStatus => lineStatus.statusSeverity != 10).length > 0
        const warningText = isHighDisruptive ? 'Service currently suffering disruptions' : 'No service disruptions'

        return (
            <div>
                <h4>{warningText}</h4>

                {isHighDisruptive &&
                    line.lineStatus.map(status => {

                        if (status.statusSeverity != 10) {
                            return (<div>{status.statusSeverityDescription}</div>)
                        }


                    })}

            </div>

        )
    }

    render() {

        const { lineDetails: { results, isSearching }, bikeLocations } = this.props;
        const { detailLine, contentView } = this.state;

        return (
            <div className={'row'}>


                <div className={'menu col-md-3'}>
                    <h4>Bike Locations</h4>
                    <Search updateView={this.updateContentView} onSearch={this.props.startSearch} />

                    <h4>Line Status Menu</h4>
                    {isSearching && <span> Is Searching</span>}
                    {results && <LineSummary onLineClick={this.onLineClick} lineDetails={results} />}
                </div>

                <div className={'contentWrapper col-md-9'}>

                    {contentView === 'BIKE_SEARCH_VIEW' &&  <BikeLocations BikeLocations={bikeLocations}/>}
                    {contentView === 'LINE_STATUS' && this.renderDetailLine(detailLine)}
                </div>

            </div>

        );
    }

}

function mapStateToProps(state) {
    return {
        lineDetails: state.lineStatus,
        bikeLocations: state.bikeLocation

    }
}

export default connect(mapStateToProps, { startRequest, startSearch })(TransportDelivery);
