import { combineReducers } from 'redux';
import {searchReducer} from './bikeSearch'
import {lineStausReducer} from './lineStatus'

export const reducers = combineReducers({
    lineStatus: lineStausReducer,
    bikeLocation: searchReducer
    })
    
    
    