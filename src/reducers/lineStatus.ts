import { handleActions, handleAction } from 'redux-actions';
import {FAILED_REQUEST, START_REQUEST, SUCESSFUL_REQUEST, ILineStatusState} from '../actions/LineStatusActions'


const initialState = {
    isSearching: false,
    searchTerm: '',
    results: null
} as ILineStatusState;


export const lineStausReducer = handleActions({
    [FAILED_REQUEST]: (state, action) => ({
        ...state,
        isSearching: false
    }),
    [START_REQUEST]: (state, action) => ({
        ...state,
        isSearching: true,
    }),
    [SUCESSFUL_REQUEST]: (state, action) => ({
        ...state,
        isSearching: false,
        results: transformArrayToObject(action.payload)
    })
}
, initialState);


const transformArrayToObject = (results) => {

    let data:any =  results.sort((a, b) => a.mode > b.mode ? 1 : -1);

    data = data.reduce((previousItems, item) => {

        if(previousItems[item.mode] === undefined)
            previousItems[item.mode] = []

        previousItems[item.mode] = previousItems[item.mode].concat(item);
        
        return previousItems

    }, {})

    return data;
}


