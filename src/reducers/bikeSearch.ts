import { handleActions, handleAction } from 'redux-actions';
import {FAILED_SEARCH, START_SEARCH, SUCESSFUL_SEARCH, IBikeSearchState, IBikeSearchResponse} from '../actions/BikeSearchActions'
import { combineReducers } from 'redux';

const initialState = {
    isSearching: false,
    searchTerm: '',
    results: [],
    previousSearchTerm: ''
} as IBikeSearchState;

export const searchReducer = handleActions({
    [FAILED_SEARCH]: (state, action) => ({
        ...state,
        isSearching: false
    }),
    [START_SEARCH]: (state, action) => ({
        ...state,
        isSearching: true,
        searchTerm: action.payload
    }),
    [SUCESSFUL_SEARCH]: (state, action) => ({
        ...state,
        isSearching: false,
        results: action.payload as IBikeSearchResponse[],
        previousSearchTerm: state.searchTerm
    })
}
, initialState);
