
import * as React from "react";
import * as _ from 'lodash'
import './main.css'

interface IAuthState {
}

interface IAuthProps {
    onSearch(searchTerm): void,
    updateView(view): void
}


class Search extends React.Component<IAuthProps, IAuthState>{

    private searchTerm;

    constructor(props, state){
        super(props, state);
        this.searchTerm = React.createRef();
        this.onSearchClick = this.onSearchClick.bind(this);
    }


    onSearchClick = () => {
        this.props.updateView('BIKE_SEARCH_VIEW')
        this.props.onSearch(this.searchTerm.current.value)
    }

    render(){


        return(
            <div className={'Search-wrapper'}>

                <div>
                    <input ref={this.searchTerm} type={"text"}/>
                </div>

                <div>
                    <button onClick={this.onSearchClick}> Search </button>
                </div>

            </div>

        );
    }

}


export default Search;
