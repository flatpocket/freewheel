
import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from 'react-redux';
import store from './store'
import TransportDelivery from './containers/TransportDelivery'
import './main.css'

import { BrowserRouter, Route, Switch } from 'react-router-dom'


const App  = () => <Provider store={store}>
<BrowserRouter history={ history }>
  <Switch   >
    <Route path="/"  component={TransportDelivery} />
    {/* <Route path="/" component={CycleHire} /> */}
  </Switch>

</BrowserRouter>
</Provider>;

ReactDOM.render(<App  />, document.getElementById("app"));