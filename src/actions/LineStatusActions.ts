import { createAction } from 'redux-actions';


export interface ILineResponse {
    _id: string
    name: string
    mode: string
    nightShift: boolean
    lineStatus: boolean
}

export interface ILineStatusState {
    searchTerm: string,
    isSearching: boolean,
    results: Object
}




export const START_REQUEST: string = 'StartRequest';
export const startRequest = createAction(START_REQUEST);

export const FAILED_REQUEST: string = 'FailedRequest';
export const failedRequest = createAction(FAILED_REQUEST);

export const SUCESSFUL_REQUEST: string = 'SuccessfulRequest';
export const successfulRequest = createAction(SUCESSFUL_REQUEST);