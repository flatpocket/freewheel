import { createAction } from 'redux-actions';


export interface IBikeSearchResponse {
    _id: string
    name: string
    lag: string
    lon: boolean
}

export interface IBikeSearchState {
    searchTerm: string,
    isSearching: boolean,
    results: IBikeSearchResponse[],
    previousSearchTerm: string
}


export const START_SEARCH: string = 'StartSearch';
export const startSearch = createAction(START_SEARCH);

export const FAILED_SEARCH: string = 'FailedSearch';
export const failedSearch= createAction(FAILED_SEARCH);

export const SUCESSFUL_SEARCH: string = 'SuccessfulSearch';
export const successfulSearch = createAction(SUCESSFUL_SEARCH);
